# Desafio para vaga - Desenvolvedor front-end #
### Sobre ###
O desafio consiste em implementar uma aplicação, que rode de forma consistente nos navegadores mais recentes e que seja responsiva para melhor visualização em dispositivos móveis.
### Regras ###

Baseado no Layout, desenvolva uma landing page consumindo os dados do json apresentado na pasta `api`.

Premissas:

1. Desenvolver uma aplicação responsiva (não utilizar framework).
2. Consumir todos os dados disponibilizados na pasta `api` para criar as prateleiras de produtos.
3. Funcionalidade slider só nas vitrines de produtos, o css das setas fica ao seu criterio: Utilizar `slick (https://kenwheeler.github.io/slick/)` ou `swiper (https://swiperjs.com/)`.
3. Funcionalidade adicionar ao carrinho: Deve salvando os itens no `localStorage` e aplicando automaticamente ao total no icone do carrinho.
4. Funcionalidade quantidade: Deve permitir o usuário adicionar mais um item ou remover mais um item do localStorage.
5. Validação de estoque: Caso não tenha quantidade disponível (available) apresentar o botão `indisponível` sem interação. 

Note que o Layout e as premissas não deixam claro todas as situações possíveis para os dados do usuário. Você pode interpretar como quiser o que não foi definido como premissa e melhorar a funcionalidade da página, caso ache necessário.
### Recomendações ###

* Utilizar pre-processadores: `Sass`
* Utilizar `Javascript ES6 Jquery`
* Utilizar automatizador de tarefas: `Gulp`, `Grunt` ou `Webpack`
* Utilizar boas práticas de desenvolvimento
* Atente-se a resposividade da tela (principalmente no formato mobile)
* Criar um `README.md` de como rodar seu projeto

Layout: https://xd.adobe.com/view/a54fa1e8-3f71-4df0-abb2-ef4ec6c23192-f01c/
...

* Tudo pronto? Basta enviar o link do seu repositório no github para o e-mail `fulano-de-tal@penseavanti.com.br` e aguardar a resposta ;)